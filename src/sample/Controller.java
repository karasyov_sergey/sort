package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Collections;

@SuppressWarnings("unused")
public class Controller {
    public TextField inputField;
    public TextArea outputField;
    private ArrayList<Integer> numbers;

    public void createArray(ActionEvent actionEvent) {
        numbers = new ArrayList<>();
        for (String number : inputField.getText().split(",")) {
            numbers.add(Integer.valueOf(number.trim()));
        }
    }

    public void clearArray(ActionEvent actionEvent) {
        numbers = null;
    }

    public void deleteMin(ActionEvent actionEvent) {
        Integer minNumber = getMinFromArray();
        numbers.removeAll(Collections.singleton(minNumber));
    }

    private int getMinFromArray() {
        int min = Integer.MAX_VALUE;
        for (Integer number : numbers) {
            if (number < min) {
                min = number;
            }
        }
        return min;
    }

    public void printArray(ActionEvent actionEvent) {
        outputField.clear();
        if (numbers != null && !numbers.isEmpty()) {
            for (int i = 0; i < numbers.size(); i++) {
                if (i != numbers.size() - 1) {
                    outputField.setText(outputField.getText() + numbers.get(i) + ", ");
                } else {
                    outputField.setText(outputField.getText() + numbers.get(i));
                }
            }
        } else {
            outputField.setText("Массив чисел пустой");
        }
    }
}
